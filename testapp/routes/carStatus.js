var express = require('express');
var router = express.Router();
var zlib = require('zlib');
var fs = require('fs');

var file = __dirname+'/carStatus.json';

// db.json をZip化して返却
router.get('/', function(req, res) {
  var content = fs.readFileSync(file);
  zlib.gzip(content, function(_, result){
    res.set({'Content-Type': 'application/json'});
    res.set({'Content-Encoding': 'gzip'});
    res.send(result);
  })
});

module.exports =router;